package com.rybacki.newsbrowser;

import com.rybacki.newsbrowser.model.NewsApiResponse;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import java.net.URI;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class NewsBrowserServiceTest {
    private static String TEST_COUNTRY = "pl";
    private static String TEST_CATEGORY = "technology";
    private static String TEST_API_KEY = "testApiKey";
    private static String TEST_PAGE_SIZE = "5";
    private static String TEST_PAGE = "1";

    private NewsBrowserService sut;

    @Mock
    private NewsApiResponse response;
    @Mock
    private NewsApiOrgRestClient client;
    @Mock
    private NewsBrowserMapper mapper;

    @Before
    public void setUp() {
        initMocks(this);
        sut = new NewsBrowserService(client, mapper, TEST_API_KEY);
    }

    @Test
    public void shouldReturnCorrectNewsApiOrgUri() {
        when(client.getNews(any())).thenReturn(response);

        sut.getNews(TEST_COUNTRY, TEST_CATEGORY, TEST_PAGE_SIZE, TEST_PAGE);

        ArgumentCaptor<URI> argument = ArgumentCaptor.forClass(URI.class);
        verify(client).getNews(argument.capture());
        assertEquals("https://newsapi.org/v2/top-headlines?country=" + TEST_COUNTRY + "&category=" + TEST_CATEGORY +
                "&apiKey=" + TEST_API_KEY + "&pageSize=" + TEST_PAGE_SIZE + "&page=" + TEST_PAGE, argument.getValue().toString());
    }
}