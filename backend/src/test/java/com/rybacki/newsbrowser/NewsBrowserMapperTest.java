package com.rybacki.newsbrowser;

import com.rybacki.newsbrowser.model.NewsApiArticle;
import com.rybacki.newsbrowser.model.NewsApiResponse;
import com.rybacki.newsbrowser.model.NewsBrowserArticle;
import com.rybacki.newsbrowser.model.NewsBrowserResponse;
import org.junit.Before;
import org.junit.Test;
import org.mapstruct.factory.Mappers;

import java.time.LocalDate;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class NewsBrowserMapperTest {
    private static final String TEST_COUNTRY = "pl";
    private static final String TEST_CATEGORY = "technology";
    private static final String TEST_SOURCE_NAME = "sourceName";
    private static final String TEST_TITLE = "title";
    private static final String TEST_AUTHOR = "author";
    private static final String TEST_DESCRIPTION = "description";
    private static final LocalDate TEST_DATE = LocalDate.parse("2015-01-02");
    private static final String TEST_URL = "url";
    private static final String TEST_URL_TO_IMAGE = "urlToImage";

    private static final NewsApiArticle GIVEN_ARTICLE_1 = new NewsApiArticle(TEST_SOURCE_NAME, TEST_TITLE,
            TEST_AUTHOR, TEST_DESCRIPTION, TEST_DATE, TEST_URL, TEST_URL_TO_IMAGE);

    private static final String EXPECTED_DATE = "2015-01-02";
    private static final NewsBrowserArticle EXPECTED_ARTICLE_1 = new NewsBrowserArticle(TEST_AUTHOR, TEST_TITLE,
            TEST_DESCRIPTION, EXPECTED_DATE, TEST_SOURCE_NAME, TEST_URL, TEST_URL_TO_IMAGE);


    private NewsBrowserMapper sut;

    @Before
    public void setup() {
        sut = Mappers.getMapper(NewsBrowserMapper.class);
    }

    @Test
    public void shouldReturnNewsBrowserResponseWhenIsGivenNewsApiResponse() {
        NewsApiResponse entity = new NewsApiResponse(Arrays.asList(GIVEN_ARTICLE_1, GIVEN_ARTICLE_1));

        NewsBrowserResponse entityResponse = sut.newsApiOrgToNewsBrowser(entity, TEST_COUNTRY, TEST_CATEGORY);

        assertEquals(TEST_COUNTRY, entityResponse.getCountry());
        assertEquals(TEST_CATEGORY, entityResponse.getCategory());
        assertEquals(EXPECTED_ARTICLE_1, entityResponse.getArticles().get(0));
        assertEquals(EXPECTED_ARTICLE_1, entityResponse.getArticles().get(1));
    }
}