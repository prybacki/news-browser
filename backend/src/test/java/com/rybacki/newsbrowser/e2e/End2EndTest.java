package com.rybacki.newsbrowser.e2e;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rybacki.newsbrowser.model.ErrorResponse;
import com.rybacki.newsbrowser.model.NewsBrowserArticle;
import com.rybacki.newsbrowser.model.NewsBrowserResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.anything;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withServerError;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class End2EndTest {
    private static final String TEST_COUNTRY = "pl";
    private static final String TEST_CATEGORY = "technology";
    private static final String TEST_SOURCE_NAME = "sourceName";
    private static final String TEST_TITLE = "title";
    private static final String TEST_AUTHOR = "author";
    private static final String TEST_DESCRIPTION = "description";
    private static final String TEST_DATE = "2019-04-02";
    private static final String TEST_URL = "url";
    private static final String TEST_URL_TO_IMAGE = "urlToImage";

    private static final String TEST_ERROR_STATUS = "error";
    private static final String TEST_ERROR_CODE = "parametersMissing";
    private static final String TEST_ERROR_MESSAGE = "Required parameters are missing";

    private static final NewsBrowserArticle TEST_NEWS_BROWSER_ARTICLE = new NewsBrowserArticle(TEST_AUTHOR,
            TEST_TITLE, TEST_DESCRIPTION, TEST_DATE, TEST_SOURCE_NAME, TEST_URL, TEST_URL_TO_IMAGE);
    private static final NewsBrowserResponse TEST_NEWS_BROWSER_RESPONSE = new NewsBrowserResponse(TEST_COUNTRY,
            TEST_CATEGORY, Arrays.asList(TEST_NEWS_BROWSER_ARTICLE));

    private static final String MOCK_SERVER_SUCCESS_RESPONSE = "{" +
            "\"status\":\"ok\"," +
            "\"totalResults\":1," +
            "\"articles\":[{" +
            "\"source\":{\"id\":null," +
            "\"name\":\"sourceName\"}," +
            "\"author\":\"author\"," +
            "\"title\":\"title\"," +
            "\"description\":\"description\"," +
            "\"url\":\"url\"," +
            "\"urlToImage\":\"urlToImage\"," +
            "\"publishedAt\":\"2019-04-02T14:57:03Z\"" +
            "}]}";

    private static final String MOCK_SERVER_ERROR_RESPONSE = "{" +
            "\"status\":\"error\"," +
            "\"code\":\"parametersMissing\"," +
            "\"message\":\"Required parameters are missing\"" +
            "}";

    private static final String REQUEST_URL = "/news/" + TEST_COUNTRY + "/" + TEST_CATEGORY;

    @Autowired
    private WebTestClient webClient;
    @Autowired
    private RestTemplate restTemplate;

    private MockRestServiceServer mockServer;
    private ObjectMapper jsonMapper;

    @Before
    public void setUp() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
        jsonMapper = new ObjectMapper();
    }

    @Test
    public void shouldReturnNewsBrowserResponse() throws JsonProcessingException {
        mockServer.expect(anything()).andExpect(method(HttpMethod.GET)).andRespond(withSuccess(MOCK_SERVER_SUCCESS_RESPONSE,
                MediaType.APPLICATION_JSON_UTF8));

        EntityExchangeResult<String> result = webClient.get()
                .uri(REQUEST_URL)
                .exchange().expectStatus().isOk()
                .expectBody(String.class).returnResult();

        mockServer.verify();
        assertEquals(jsonMapper.writeValueAsString(TEST_NEWS_BROWSER_RESPONSE), result.getResponseBody());
    }

    @Test
    public void shouldErrorResponseWhenServerError() throws JsonProcessingException {
        mockServer.expect(anything()).andExpect(method(HttpMethod.GET)).andRespond(withServerError().body(MOCK_SERVER_ERROR_RESPONSE));
        ErrorResponse expectedResponse = new ErrorResponse(TEST_ERROR_STATUS, TEST_ERROR_CODE, TEST_ERROR_MESSAGE);

        EntityExchangeResult<String> result = webClient.get()
                .uri(REQUEST_URL)
                .exchange().expectStatus().is5xxServerError()
                .expectBody(String.class).returnResult();

        mockServer.verify();
        assertEquals(jsonMapper.writeValueAsString(expectedResponse), result.getResponseBody());
    }

    @Test
    public void shouldErrorResponseWhenMethodIsNotSupported() throws JsonProcessingException {
        ErrorResponse expectedResponse = new ErrorResponse(TEST_ERROR_STATUS, HttpStatus.METHOD_NOT_ALLOWED.toString(),
                HttpStatus.METHOD_NOT_ALLOWED.getReasonPhrase());

        EntityExchangeResult<String> result = webClient.post()
                .uri(REQUEST_URL)
                .exchange().expectStatus().is4xxClientError()
                .expectBody(String.class).returnResult();

        assertEquals(jsonMapper.writeValueAsString(expectedResponse), result.getResponseBody());
    }
}
