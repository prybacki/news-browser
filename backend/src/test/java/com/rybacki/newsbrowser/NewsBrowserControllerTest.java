package com.rybacki.newsbrowser;

import com.rybacki.newsbrowser.model.ErrorResponse;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;

import static org.junit.Assert.assertEquals;
import static org.mockito.MockitoAnnotations.initMocks;

public class NewsBrowserControllerTest {
    private static final String MOCK_SERVER_ERROR_RESPONSE = "{" +
            "\"status\":\"error\"," +
            "\"code\":\"parametersMissing\"," +
            "\"message\":\"Required parameters are missing\"" +
            "}";

    private static final String EXPECTED_TEST_CODE = "parametersMissing";
    private static final String EXPECTED_TEST_MESSAGE = "Required parameters are missing";

    @Mock
    private NewsBrowserService service;

    private NewsBrowserController sut;

    @Before
    public void setup() {
        initMocks(this);
        sut = new NewsBrowserController(service);
    }

    @Test
    public void shouldSerializeErrorResponseWhenErrorBodyExist() {
        HttpClientErrorException ex = new HttpClientErrorException(HttpStatus.NOT_FOUND, "message",
                MOCK_SERVER_ERROR_RESPONSE.getBytes(), null);
        ResponseEntity<ErrorResponse> response = sut.onHttpStatusCodeException(ex);

        assertEquals(EXPECTED_TEST_CODE, response.getBody().getCode());
        assertEquals(EXPECTED_TEST_MESSAGE, response.getBody().getMessage());
    }

    @Test
    public void shouldSetHttpCodeAndReasonWhenErrorBodyCannotBeSerialized() {
        HttpClientErrorException ex = new HttpClientErrorException(HttpStatus.NOT_FOUND, "message", null, null);
        ResponseEntity<ErrorResponse> response = sut.onHttpStatusCodeException(ex);

        assertEquals(HttpStatus.NOT_FOUND.toString(), response.getBody().getCode());
        assertEquals(HttpStatus.NOT_FOUND.getReasonPhrase(), response.getBody().getMessage());
    }
}