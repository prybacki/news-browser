package com.rybacki.newsbrowser.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewsBrowserArticle {
    @ApiModelProperty(notes = "The author of the article")
    @JsonProperty("author")
    private String author;

    @ApiModelProperty(notes = "The headline or title of the article. ")
    @JsonProperty("title")
    private String title;

    @ApiModelProperty(notes = "A description or snippet from the article. ")
    @JsonProperty("description")
    private String description;

    @ApiModelProperty(notes = "The date and time that the article was published, in UTC (+000) ")
    @JsonProperty("date")
    private String date;

    @ApiModelProperty(notes = "The display name for the source this article came from")
    @JsonProperty("sourceName")
    private String sourceName;

    @ApiModelProperty(notes = "The direct URL to the article")
    @JsonProperty("articleUrl")
    private String articleUrl;

    @ApiModelProperty(notes = "The URL to a relevant image for the article. ")
    @JsonProperty("imageUrl")
    private String imageUrl;
}
