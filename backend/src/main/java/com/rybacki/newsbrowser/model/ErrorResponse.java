package com.rybacki.newsbrowser.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ErrorResponse {
    @ApiModelProperty(notes = "If the request was successful or not. Options: ok, error. In the case of error a code " +
            "and message property will be populated.")
    @JsonProperty("status")
    private String status;

    @ApiModelProperty(notes = "A short code identifying the type of error returned. ")
    @JsonProperty("code")
    private String code;

    @ApiModelProperty(notes = "A fuller description of the error, usually including how to fix it.")
    @JsonProperty("message")
    private String message;
}

