package com.rybacki.newsbrowser.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewsBrowserResponse {
    @ApiModelProperty(notes = "The 2-letter ISO 3166-1 code of the country you get headlines for.")
    @JsonProperty("country")
    private String country;

    @ApiModelProperty(notes = "The category you get headlines for.")
    @JsonProperty("category")
    private String category;

    @ApiModelProperty(notes = "The results of the request.")
    @JsonProperty("articles")
    private List<NewsBrowserArticle> articles;
}
