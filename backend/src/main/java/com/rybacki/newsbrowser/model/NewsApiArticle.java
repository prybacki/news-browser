package com.rybacki.newsbrowser.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;
import java.util.Map;

@Getter
@AllArgsConstructor
public class NewsApiArticle {
    private String sourceName;

    @JsonProperty("title")
    private String title;

    @JsonProperty("author")
    private String author;

    @JsonProperty("description")
    private String description;

    @JsonProperty("publishedAt")
    private LocalDate publishedAt;

    @JsonProperty("url")
    private String url;

    @JsonProperty("urlToImage")
    private String urlToImage;

    @JsonProperty("source")
    private void getNameFromSource(Map<String, String> source) {
        sourceName = source.get("name");
    }
}
