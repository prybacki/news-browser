package com.rybacki.newsbrowser.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class NewsApiResponse {
    @JsonProperty("articles")
    private List<NewsApiArticle> articles;
}
