package com.rybacki.newsbrowser;

import com.rybacki.newsbrowser.model.NewsApiArticle;
import com.rybacki.newsbrowser.model.NewsApiResponse;
import com.rybacki.newsbrowser.model.NewsBrowserArticle;
import com.rybacki.newsbrowser.model.NewsBrowserResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public abstract class NewsBrowserMapper {

    @Mapping(source = "response.articles", target = "articles")
    @Mapping(source = "country", target = "country")
    @Mapping(source = "category", target = "category")
    abstract NewsBrowserResponse newsApiOrgToNewsBrowser(NewsApiResponse response, String country, String category);

    @Mapping(source = "author", target = "author")
    @Mapping(source = "title", target = "title")
    @Mapping(source = "description", target = "description")
    @Mapping(source = "publishedAt", target = "date", dateFormat = "yyyy-MM-dd")
    @Mapping(source = "sourceName", target = "sourceName")
    @Mapping(source = "url", target = "articleUrl")
    @Mapping(source = "urlToImage", target = "imageUrl")
    abstract NewsBrowserArticle newsApiArticleToNewsBrowserArticle(NewsApiArticle article);
}
