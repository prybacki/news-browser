package com.rybacki.newsbrowser;

import com.rybacki.newsbrowser.model.NewsApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Component
public class NewsApiOrgRestClient {

    @Autowired
    private RestTemplate restTemplate;

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    public NewsApiResponse getNews(URI uri) {
        RequestEntity request = RequestEntity.get(uri).accept(MediaType.APPLICATION_JSON_UTF8).build();
        return restTemplate.exchange(request, NewsApiResponse.class).getBody();
    }
}
