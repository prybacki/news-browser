package com.rybacki.newsbrowser;

import com.rybacki.newsbrowser.model.NewsApiResponse;
import com.rybacki.newsbrowser.model.NewsBrowserResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriTemplate;

import java.net.URI;

@Service
public class NewsBrowserService {

    private final NewsApiOrgRestClient client;
    private final NewsBrowserMapper newsBrowserMapper;
    private String apiKey;
    private final String uriTemplate = "https://newsapi.org/v2/top-headlines?country={country}&category={category" +
            "}&apiKey={apiKey}&pageSize={pageSize}&page={page}";


    @Autowired
    public NewsBrowserService(NewsApiOrgRestClient client, NewsBrowserMapper newsBrowserMapper,
                              @Value("${api.key}") String apiKey) {
        this.client = client;
        this.newsBrowserMapper = newsBrowserMapper;
        this.apiKey = apiKey;
    }

    public NewsBrowserResponse getNews(String country, String category, String pageSize, String page) {
        URI uri = new UriTemplate(uriTemplate).expand(country, category, apiKey, pageSize, page);
        NewsApiResponse response = client.getNews(uri);
        return newsBrowserMapper.newsApiOrgToNewsBrowser(response, country, category);
    }
}
