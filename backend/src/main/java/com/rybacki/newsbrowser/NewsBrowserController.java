package com.rybacki.newsbrowser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rybacki.newsbrowser.model.ErrorResponse;
import com.rybacki.newsbrowser.model.NewsBrowserResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpStatusCodeException;

import java.io.IOException;

@RequestMapping("/news")
@RestController
@CrossOrigin("*")
public class NewsBrowserController {
    private static final String STATUS_ERROR = "error";

    private final NewsBrowserService service;

    @Autowired
    public NewsBrowserController(NewsBrowserService service) {
        this.service = service;
    }

    @ApiOperation(value = "Fetch news for given country and category", response = NewsBrowserResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK. The request was executed successfully."),
            @ApiResponse(code = 400, message = "Bad Request. The request was unacceptable, often due to a missing or " +
                    "misconfigured parameter."),
            @ApiResponse(code = 401, message = "Unauthorized. Your API key was missing from the request, or wasn't " +
                    "correct."),
            @ApiResponse(code = 429, message = "Too Many Requests. You made too many requests within a window of time" +
                    " and have been rate limited. Back off for a while."),
            @ApiResponse(code = 500, message = "Server Error. Something went wrong on our side.")
    })

    @GetMapping("/{country}/{category}")
    public NewsBrowserResponse getNews(
            @ApiParam(value = "The 2-letter ISO 3166-1 code of the country you want to get headlines for",
                    allowableValues = "ae, ar, at, au, be, bg, br, ca, ch, cn, co, cu, cz, de, eg, fr, gb, gr, hk, " +
                            "hu, id, ie, il, in, it, jp, kr, lt, lv, ma, mx, my, ng, nl, no, nz, ph, pl, pt, ro, rs, " +
                            "ru, sa, se, sg, si, sk, th, tr, tw, ua, us, ve, za", required = true)
            @PathVariable("country") String country,
            @ApiParam(value = "The category you want to get headlines for.", allowableValues = "business, " +
                    "entertainment, " +
                    "general, health, science, sports, technology", required = true)
            @PathVariable("category") String category,
            @ApiParam(value = "The number of results to return per page (request). 100 is the maximum.", defaultValue = "20")
            @RequestParam(required = false, defaultValue = "20") String pageSize,
            @ApiParam(value = "Use this to page through the results if the total results found is greater than the page size.", defaultValue = "1")
            @RequestParam(required = false, defaultValue = "1") String page) {
        return service.getNews(country, category, pageSize, page);
    }

    @ApiOperation(value = "", hidden = true)
    @RequestMapping(value = "/**")
    public ResponseEntity<ErrorResponse> onNotSupportedMethod() {
        return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).body(new ErrorResponse(STATUS_ERROR,
                HttpStatus.METHOD_NOT_ALLOWED.toString(), HttpStatus.METHOD_NOT_ALLOWED.getReasonPhrase()));
    }

    @ExceptionHandler(HttpStatusCodeException.class)
    public ResponseEntity<ErrorResponse> onHttpStatusCodeException(HttpStatusCodeException ex) {
        ObjectMapper mapper = new ObjectMapper();
        ErrorResponse errorResponse;
        try {
            errorResponse = mapper.readValue(ex.getResponseBodyAsByteArray(), ErrorResponse.class);
        } catch (IOException e) {
            errorResponse = new ErrorResponse(STATUS_ERROR, ex.getStatusCode().toString(),
                    ex.getStatusCode().getReasonPhrase());
        }
        return ResponseEntity.status(ex.getStatusCode()).body(errorResponse);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> onException() {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(STATUS_ERROR,
                HttpStatus.INTERNAL_SERVER_ERROR.toString(), HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase()));
    }
}
