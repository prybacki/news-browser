import {Component, OnInit, OnDestroy, ViewChild, ChangeDetectorRef} from '@angular/core';
import {Observable} from 'rxjs';
import {MatTableDataSource, MatPaginator} from '@angular/material';
import {Article} from "../model/article.model";
import {NewsBrowserService} from '../news-browser.service';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.scss']
})
export class NewsListComponent implements OnInit, OnDestroy {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  obs: Observable<Article[]>;
  dataSource: MatTableDataSource<Article>;

  readonly NEWS_MAX = 70; //for demo purpose, maximum number of message visible for developer account on news api org.
  readonly INITIAL_PAGE_SIZE = 5;

  constructor(private newsBrowserService: NewsBrowserService, private changeDetectorRef: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.newsBrowserService.getAll(this.INITIAL_PAGE_SIZE, 1).subscribe(data => {
      this.dataSource = new MatTableDataSource<Article>(data.articles);
      this.dataSource.data.length = this.NEWS_MAX; //very ugly method to fix size of available news
      this.dataSource.paginator = this.paginator;
      this.changeDetectorRef.detectChanges();
      this.obs = this.dataSource.connect();
    });
  }

  ngOnDestroy() {
    if (this.dataSource) {
      this.dataSource.disconnect();
    }
  }

  onPaginateChange(event) {
    this.newsBrowserService.getAll(event.pageSize, event.pageIndex + 1).subscribe(data => {
      this.dataSource = new MatTableDataSource<Article>(data.articles);
      this.obs = this.dataSource.connect();
    });
  }
}