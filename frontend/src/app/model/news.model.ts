import {Article} from "./article.model";

export class News{
    country: string;
    category: string;
    articles: Article[];
}