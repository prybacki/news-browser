import { TestBed } from '@angular/core/testing';

import { NewsBrowserService } from './news-browser.service';

describe('NewsBrowserService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NewsBrowserService = TestBed.get(NewsBrowserService);
    expect(service).toBeTruthy();
  });
});
