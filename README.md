# News Browser with Spring Boot and Angular
 
**Prerequisites:** [Java 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html).

## Getting Started

To install this example application, run the following commands:

```bash
git clone https://github.com/prybacki/news-browser.git
cd news-browser
```

This will get a copy of the project installed locally. To install all of its dependencies and start each app, follow the instructions below.

### In order to build and deploy to Docker:
```bash
./mvnw clean install
```

After that in your local Docker repository will be created two images:
- news-browser-backend
- news-browser-frontend

To run them:
```bash
./docker-compose up
```

Run browser and go to site: http://localhost:8081

### To build and run without Docker
Start backend application:
```bash
cd backend
./mvn spring-boot:run
```

Start frontend application:
```bash
cd frontend
./ng serve --port 8081
```

Run browser and go to site: http://localhost:8081

## Documentation
Documentation for backend application is available after application start under: http://localhost:8080/swagger-ui.html
